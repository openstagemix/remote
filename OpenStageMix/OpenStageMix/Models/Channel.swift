//
//  Channel.swift
//  OpenStageMix
//
//  Created by Derick Mathews on 4/5/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import SwiftUI

struct Channel: Hashable, Codable, Identifiable {
    var id: Int
    var name:String
    fileprivate var imageName: String
    var isFavorite: Bool
    var volume: Float
    enum Category: String, CaseIterable, Codable, Hashable {
        case singer = "Singer"
        case musician = "Musician"
        case speaker = "Speaker"
    }
}

extension Channel {
    var image: Image {
        ImageStore.shared.image(name: imageName)
    }
}
