/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A view that clips an image to a circle and adds a stroke and shadow.
*/

import SwiftUI

struct CircleImage: View {
    var image: Image
    var body: some View {
        VStack {
            image
                .resizable()
                .scaleEffect(0.7)
//                .clipShape(Circle()
                .contentShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                .overlay(
                    Circle().stroke(Color.white, lineWidth: 10))
                .shadow(radius: 8)
                .imageScale(.small)
                .aspectRatio(contentMode: .fit)
                .allowsTightening(/*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                
                
                
            Spacer()
        }
    .padding()
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage(image: channelData[0].image)
    }
}
