//
//  ChannelList.swift
//  OpenStageMix
//
//  Created by Derick Mathews on 4/6/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import SwiftUI

struct ChannelList: View {
    @EnvironmentObject private var userData: UserData
    var body: some View {
        NavigationView
        {
            List {
            Toggle(isOn: $userData.showFavoritesOnly) {
                Text("Show Favorites Only")
            }
            
            ForEach(userData.channels) { channel in
                if !self.userData.showFavoritesOnly || channel.isFavorite {
                        NavigationLink(
                            destination: ChannelDetail(channel: channel)
                                .environmentObject(self.userData)
                        ) {
                            ChannelRow(channel: channel)
                        }
                    }
                }
            }
            .navigationBarTitle(Text("Open Stage Mix Remote"))
        }
        
    }
}

struct ChannelList_Previews: PreviewProvider {
    static var previews: some View {
        ChannelList()
        .environmentObject(UserData())
    }
}
