//
//  UDPServer.swift
//  OpenStageMix
//
//  Created by Derick Mathews on 7/3/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import Foundation
import Network

let StageMixService = "_stagemix._udp"

class UDPServer {
    var listener: NWListener
    var queue: DispatchQueue
    var connected: Bool = false
    
    init?() {
        queue = DispatchQueue(label: "UDP Server  Queue")
        let serverParams = NWParameters(dtls: nil, udp: NWProtocolUDP.Options())
        
        listener = try! NWListener(using: serverParams)
        
        listener.service = NWListener.Service(type: StageMixService)
            
        listener.newConnectionHandler = { [weak self] (newConncection) in
            if let strongSelf = self {
                newConncection.start(queue: strongSelf.queue)
                
                strongSelf.recieve(on: newConncection)
            }
        }
        
        listener.stateUpdateHandler = { [weak self] (newState) in
            switch (newState) {
            case .ready:
                print("Listening on port \(String(describing: self?.listener.port))")
            default:
                break
            }
        }
        listener.start(queue: queue)
    }
    
    func recieve(on connection: NWConnection) {
        
        connection.receiveMessage { (data, context, isComplete, error) in
            if let frame = data {
                if !self.connected {
                    connection.send(content: frame, completion: .idempotent)
                    print("Echoed initial content: \(frame)")
                    self.connected = true
                } else {
//                    self.controller?.recieved(frame: frame)
                }
                
                if error == nil {
                    self.recieve(on: connection)
                }
            }
        }
    }
}
