//
//  UDPClient.swift
//  OpenStageMix
//
//  Created by Derick Mathews on 7/3/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import SwiftUI
import Network

class UDPClient {
    var connection: NWConnection
    var queue: DispatchQueue
    
    init(name: String) {
        queue = DispatchQueue(label: "UDP Client Queue")
        
        connection = NWConnection(to: .service(name: name, type:"_stagemix._udp", domain:"local", interface: nil), using: .udp)
        
        connection.stateUpdateHandler = { [weak self] (newState) in
            switch(newState) {
            case .ready:
                print("Ready to send")
                self?.sendInitialFrame()
            case .failed(let error):
                print("Client failed with error: \(error)")
            default:
                break  
            }
        }
        connection.start(queue: queue)
        
    }
    
    func sendInitialFrame() {
        let helloMessage = "hello".data(using: .utf8)
        connection.send(content: helloMessage, completion: .contentProcessed({ (error) in
            if let error = error {
                print("Send error: \(error)")
            }
        }))
        
        connection.receiveMessage { (data, context, isComplete, error) in
            if data != nil {
                print("Got connected!")
//                if let controller = self.controller {
//                    controller.connected()
//                }
            }
        }
    }
    
    func send(frames: [Data]) {
        connection.batch {
            for frame in frames {
                connection.send(content: frame, completion: .contentProcessed({ (error) in
                    if let error = error {
                        print("Send error: \(error)")
                    }
                }))
                
            }
        }
    }
}
