//
//  UDPBrowser.swift
//  OpenStageMix
//
//  Created by Derick Mathews on 7/17/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import Foundation
import Network

var sharedBrowser: UDPBrowser?

// Update the UI when you receive new browser results.
protocol PeerBrowserDelegate: class {
    func refreshResults(results: Set<NWBrowser.Result>)
    func displayBrowseError(_ error: NWError)
}

class UDPBrowser {

    var browser: NWBrowser?

    // Create a browsing object with a delegate.
    init() {
        startBrowsing()
    }

    // Start browsing for services.
    func startBrowsing() {
        // Create parameters, and allow browsing over peer-to-peer link.
        let parameters = NWParameters()
        parameters.includePeerToPeer = true

        // Browse for a custom "_tictactoe._tcp" service type.
        let browser = NWBrowser(for: .bonjour(type: "_stagemix._tcp", domain: nil), using: parameters)
        self.browser = browser
        browser.stateUpdateHandler = { newState in
            switch newState {
            case .failed(let error):
                // Restart the browser if it loses its connection
                if error == NWError.dns(DNSServiceErrorType(kDNSServiceErr_DefunctConnection)) {
                    print("Browser failed with \(error), restarting")
                    browser.cancel()
                    self.startBrowsing()
                } else {
                    print("Browser failed with \(error), stopping")
                    browser.cancel()
                }
            case .ready:
                print("ready")
                // Post initial results.
            case .cancelled:
                sharedBrowser = nil
            default:
                break
            }
        }

        // When the list of discovered endpoints changes, refresh the delegate.
        browser.browseResultsChangedHandler = { results, changes in
            print("results changed")
        }

        // Start browsing and ask for updates on the main queue.
        browser.start(queue: .main)
    }
}
    
