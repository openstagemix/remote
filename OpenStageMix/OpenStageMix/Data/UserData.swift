//
//  UserData.swift
//  OpenStageMix
//
//  Created by Derick Mathews on 4/6/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

final class UserData: ObservableObject {
    @Published var showFavoritesOnly = false
    @Published var channels = channelData
}
