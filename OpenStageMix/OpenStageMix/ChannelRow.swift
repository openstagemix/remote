//
//  ChannelView.swift
//  OpenStageMix
//
//  Created by Derick Mathews on 4/6/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import SwiftUI

struct ChannelRow: View {
    var channel: Channel
    var body: some View {
        VStack(alignment: .center){
            HStack {
                channel.image
                .resizable()
                    .frame(width: 50, height: 50)
                Text(channel.name)
                    .font(.title)
                Spacer()
                
                if channel.isFavorite {
                    Image(systemName: "star.fill")
                        .imageScale(.medium)
                        .foregroundColor(.yellow)
                }
            }
            .padding()
            Text("\(((channel.volume-50)*(0.2)), specifier: "%.2f") dB")
                .font(.subheadline).foregroundColor(.green)
            
        }
    }
}

struct ChannelRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ChannelRow(channel: channelData[0])
            ChannelRow(channel: channelData[1])
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
