//
//  SingerView.swift
//  OpenStageMix
//
//  Created by Derick Mathews on 4/5/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import SwiftUI

struct Channel: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct Channel_Previews: PreviewProvider {
    static var previews: some View {
        Channel()
    }
}
