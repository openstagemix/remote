//
//  ChannelDetail.swift
//  OpenStageMix
//
//  Created by Derick Mathews on 4/6/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import SwiftUI

struct ChannelDetail: View {
    @EnvironmentObject var userData: UserData
    var channel: Channel
    var channelIndex: Int {
        userData.channels.firstIndex(where: { $0.id == channel.id})!
    }
    @State private var volume : Float = 50
    
    var body: some View {

        VStack {
            VStack(alignment: .center)
            {
                VStack(alignment: .leading)
                {
                    HStack(alignment: .center){
                        Text(channel.name)
                            .font(.title)
                        Spacer()
                        Button(action: {
                            self.userData.channels[self.channelIndex]
                                .isFavorite.toggle()
                        }) {
                            if self.userData.channels[self.channelIndex]
                                .isFavorite {
                                Image(systemName: "star.fill")
                                    .foregroundColor(Color.yellow)
                            } else {
                                Image(systemName: "star")
                                    .foregroundColor(Color.gray)
                            }
                        }
                    }.padding()
                CircleImage(image: channel.image)
                    .padding(.top, 120)
                    .offset(x: 0, y: -130)
            }.padding()
                VStack(alignment: .center){
                    CustomSlider(percentage: $volume)
                    .accentColor(.green)
                    .frame(width: 200, height: 44)
                    Text("\(((volume-50)*(0.2)), specifier: "%.2f") dB")
                        .font(.subheadline)
                        .foregroundColor(.green)
                }.padding()
            }
            .padding()
            Spacer()
        }.onDisappear {
            self.userData.channels[self.channelIndex].volume = self.volume
        }.onAppear() {
            self.volume = self.userData.channels[self.channelIndex].volume
        }
        .navigationBarTitle(Text(channel.name), displayMode: .inline)
    }
}

struct ChannelDetail_Previews: PreviewProvider {
    static var previews: some View {
        let userData = UserData()
        return ChannelDetail(channel: userData.channels[0])
            .environmentObject(userData)
    }
}
